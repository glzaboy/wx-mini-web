package com.microtf.wx.config;

import com.microtf.wx.server.BaiduAPI;
import com.microtf.wx.server.NetClient;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class Config {
    @Bean
    public BaiduAPI baiduToken(){
        return new BaiduAPI();
    }
    @Bean
    OkHttpClient okHttpClient(){
        OkHttpClient.Builder builder=new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS);
        return builder.build();
    }
//    @Bean
//    NetClient netClient(OkHttpClient okHttpClient){
//        return new NetClient(okHttpClient);
//    }
}
