package com.microtf.wx.server.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data

public class BodySeg {
    @JsonProperty("log_id")
    String logId;
    @JsonProperty("labelmap")
    String labelMap;
    @JsonProperty("scoremap")
    String scoreMap;
    @JsonProperty("foreground")
    String foreGround;
}
