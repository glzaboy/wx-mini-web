package com.microtf.wx.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

public class Image {
    static Logger logger= LoggerFactory.getLogger(Image.class);

    public static BufferedImage compress(byte[] fileByte) throws IOException {
        ByteArrayInputStream byteArrayInputStream=new ByteArrayInputStream(fileByte);
        BufferedImage read = ImageIO.read(byteArrayInputStream);
        return compress(read);
    }
    public static BufferedImage compress(File file) throws IOException {
        BufferedImage read = ImageIO.read(file);
        return compress(read);
    }
    public static BufferedImage compress(BufferedImage image) throws IOException {
        int x,y,w,h;
        x=0;
        y=0;
        w=image.getWidth();
        h=image.getHeight();
        logger.info("原始文件w{},h{}",w,h);
        for(;x<w;x++){
            int hasColor=0;
            for (int i=0; i < h; i++) {
                int rgb = image.getRGB(x, i);
                Color color = new Color(rgb, true);
                if(color.getAlpha()>0){
                    hasColor=1;
                    break;
                }
            }
            if(hasColor>0){
                break;
            }
        }
        //y++
        for(;y<h;y++){
            int hasColor=0;
            for (int i=x; i < w; i++) {
                int rgb = image.getRGB(i, y);
                Color color = new Color(rgb, true);
                if(color.getAlpha()>0){
                    hasColor=1;
                    break;
                }
            }
            if(hasColor>0){
                break;
            }
        }

        //w--
        w--;
        for(;x<w;w--){
            int hasColor=0;
            for (int i=y; i < h; i++) {
                int rgb = image.getRGB(w, i);
                Color color = new Color(rgb, true);
                if(color.getAlpha()>0){
                    hasColor=1;
                    break;
                }
            }
            if(hasColor>0){
                break;
            }
        }
        //h--
        h--;
        for(;y<h;h--){
            int hasColor=0;
            for (int i=x; i < w; i++) {
                int rgb = image.getRGB(i, h);
                Color color = new Color(rgb, true);
                if(color.getAlpha()>0){
                    hasColor=1;
                    break;
                }
            }
            if(hasColor>0){
                break;
            }
        }
        logger.info("图片内容从x{},y{},wx{},hy{},w{},h{}",x,y,w,h,w-x,h-y);
        float nh=h-y;
        float nw=w-x;
        if(nw>400.0f){
            nw=400.0f;
            nh=nw/((float) (w-x)/(h-y));
        }else if (nh>600.0f){
            nw=((float) (w-x)/(h-y))*600.0f;
        }
        logger.info("需要生成图片大小w{},h{}",nw,nh);
        BufferedImage bufferedImage=new BufferedImage((int)nw,(int)nh,BufferedImage.TYPE_INT_ARGB);
        Graphics graphics = bufferedImage.getGraphics();
        graphics.drawImage(image,0,0,(int)nw,(int)nh,x,y,w,h,null);
        return bufferedImage;
    }
}
