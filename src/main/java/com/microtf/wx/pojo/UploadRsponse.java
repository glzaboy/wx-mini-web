package com.microtf.wx.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class UploadRsponse implements Serializable {
    String id;
}
