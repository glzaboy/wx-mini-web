//package com.microtf.wx;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.MediaType;
//import org.springframework.web.reactive.function.server.RequestPredicates;
//import org.springframework.web.reactive.function.server.RouterFunction;
//import org.springframework.web.reactive.function.server.RouterFunctions;
//import org.springframework.web.reactive.function.server.ServerResponse;
//
//@Configuration
//public class CityRouter {
//    @Bean
//    public RouterFunction<ServerResponse> routeCity(CityHandler cityHandler) {
//        return RouterFunctions
//                .route(RequestPredicates.GET("/hello")
//                                .and(RequestPredicates.accept(MediaType.TEXT_PLAIN)),
//                        cityHandler::helloCity);
//    }
//    @Bean
//    public RouterFunction<ServerResponse> routeCity2(CityHandler cityHandler) {
//        return RouterFunctions
//                .route(RequestPredicates.GET("/abc")
//                                .and(RequestPredicates.accept(MediaType.TEXT_PLAIN)),
//                        cityHandler.abc);
//    }
//    @Bean
//    public RouterFunction<ServerResponse> routeCity3(CityHandler cityHandler) {
//        return RouterFunctions
//                .route(RequestPredicates.GET("/myh")
//                                .and(RequestPredicates.accept(MediaType.TEXT_PLAIN)),
//
//                        cityHandler.myh);
//    }
//}
